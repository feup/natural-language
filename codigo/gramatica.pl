/*
Perguntas exemplo:
- Qual a universidade com melhor media?
- Que universidades do norte?
- Quais os cursos mais recentes?
- Qual o curso com pior media?
- Quais (são) as universidades do Porto?
- Quais (são) os cursos da FEUP?
- E da universidade FCUP?
- Quais os cursos com média superior a 15 e inferior a 18?
- Quais os cursos de universidades nortenhas com média superior a 15?
- Quantos são os cursos de engenharia?
- A universidade do Porto possui o curso de Mestrado Integrado em Engenharia Informatica e de Computacao.
*/
%      pron_int(_-_, __, tipo, det-obrigatorio?, verbo+det?)
pron_int(_-_, ql-0, 0, 0) --> [que].
pron_int(_-s, ql-1, 1, 1) --> [qual].
pron_int(_-p, ql-1, 1, 1) --> [quais].
pron_int(f-p, qt-2, 0, 1) --> [quantas].
pron_int(m-p, qt-2, 0, 1) --> [quantos].

verbo(p,ser) --> [sao].
verbo(s,ser) --> [e].
verbo(_,ter) --> [tem].
verbo(p,possuir) --> [possuem].
verbo(s,possuir) --> [possui].

det(m-s) --> [o].
det(m-p) --> [os].
det(f-s) --> [a].
det(f-p) --> [as].

nome(_-s, regiao) --> [regiao].
nome(_-p, regiao) --> [regioes].
nome(_-s, universidade) --> [universidade].
nome(_-p, universidade) --> [universidades].
nome(_-s, faculdade) --> [faculdade].
nome(_-p, faculdade) --> [faculdades].
nome(_-s, curso) --> [curso].
nome(_-p, curso) --> [cursos].
nome(_-s, 'media<') --> [media, mais, baixa].
nome(_-p, 'media<') --> [medias, mais, baixas].
nome(_-s, 'media>') --> [media, mais, alta].
nome(_-p, 'media>') --> [medias, mais, altas].
nome(_-s, media) --> [media].
nome(_-p, media) --> [medias].

contr(i-_, em) --> [em].
contr(m-s, em) --> [no].
contr(f-s, em) --> [na].
contr(m-p, em) --> [nos].
contr(f-p, em) --> [nas].
contr(i-_, de) --> [de].
contr(m-s, de) --> [do].
contr(f-s, de) --> [da].
contr(m-p, de) --> [dos].
contr(f-p, de) --> [das].

% Numero de Palavras indefinido?
% Let me present you the subset

subset0([], X, X).
subset0([X], [X | T], []) :-
    var(T),
    !.
subset0([X | N], [X | Y], R) :-
    subset0(N, Y, R).

subset(X, In, Out) :-
    subset0(Res, In, Out),
    length(Res, 1), !,
    Res=[X].
subset(Res, In, Out) :-
    subset0(Res, In, Out).

pref_atributo(regiao) --> det(f-s), [regiao].
pref_atributo(universidade) --> det(f-s), [universidade].
pref_atributo(faculdade) --> det(f-s), [faculdade].
pref_atributo(curso) --> det(m-s), [curso].
pref_atributo(tipo) --> fail.

atributo(regiao, G-N, _, X) --> subset(X), { regiao(G-N, X) }.
atributo(regiao, G-N, _, X) --> subset(X), { distrito(G-N, X, _) }.
atributo(universidade, G-N, _, S) --> subset(X), { universidade(G-N, L, _), member(X, L), nth0(0, L, S) }.
atributo(faculdade, G-N, _, S) --> subset(X), { faculdade(G-N, L, _), member(X, L), nth0(0, L, S) }.
atributo(curso, G-N, _, S) --> subset(X), { curso(G-N, L, _), member(X, L), nth0(0, L, S) }.
atributo(tipo, G-N, de, X) --> subset(X), { tipo(G-N, X) }.

relacao([q, fa], regiao, universidade).
relacao([q, fa], regiao, faculdade).
relacao([q, fa], regiao, curso).
relacao([q, fa], universidade, regiao).
relacao([fa], universidade, faculdade).
relacao([fa], universidade, curso).
relacao([q, fa], faculdade, regiao).
relacao([q, fa], faculdade, universidade).
relacao([q, fa], faculdade, tipo).
relacao([fa], faculdade, curso).
relacao([q, fa], curso, regiao).
relacao([q, fa], curso, universidade).
relacao([q, fa], curso, faculdade).
relacao([q, fa], curso, tipo).
relacao([q, fa], curso, 'media<').
relacao([q, fa], curso, 'media>').
relacao([q, fa], curso, media).
relacao([q], media, curso).
relacao([q], 'media<', regiao).
relacao([q], 'media>', regiao).
relacao([q], 'media<', universidade).
relacao([q], 'media>', universidade).
relacao([q], 'media<', faculdade).
relacao([q], 'media>', faculdade).

adjectivo(m-p,regiao-norte) --> [nortenhos].
adjectivo(m-s,regiao-norte) --> [nortenho].
adjectivo(f-p,regiao-norte) --> [nortenhas].
adjectivo(f-s,regiao-norte) --> [nortenha].
adjectivo(_-p,regiao-centro) --> [centrais].
adjectivo(_-s,regiao-centro) --> [central].

%atributo(_-_, faculdade, X) --> [X], { faculdade(_, L), member(X, L) }.
%atributo(_-_, curso, X) --> [X], { curso(_, L), member(X, L) }.


