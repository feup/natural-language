looper :-
    le_frase,
    fail.
looper :-
    looper.

removePontuacao([], []).
removePontuacao([H | T], NL) :-
    removePontuacao(T, NL0),
    (
        (\+ pontuacao(H), !, append([H], NL0, NL));
        (append([], NL0, NL))
    ).

lista_de_palavras([X|Y]) -->
    palavra(X),
    espaco_branco,
    lista_de_palavras(Y).
lista_de_palavras([X]) -->
    espaco_branco,
    lista_de_palavras(X).
lista_de_palavras([X]) -->
    palavra(X).
lista_de_palavras([X]) -->
    palavra(X),
    espaco_branco.

palavra(W) -->
    lista_chars(X),
    { name(W,X) }.

lista_chars([X|Y]) -->
    char(X),
    lista_chars(Y).
lista_chars([X]) -->
    char(X).

char(Y) --> [X], { X >= 65, X =< 90, Y is X+32}.
char(X) --> [X], { X >= 97, X =< 122}.
char(X) --> [X], { X >= 48, X =< 57}.
char(97 ) --> [X], { member(X, [225,290,227,226,193,192,195,194]) }.
char(101) --> [X], { member(X, [233,232,234,201,200,202]) }.
char(105) --> [X], { member(X, [237,236,205,204]) }.
char(111) --> [X], { member(X, [245,213,243,211,242,210]) }.
char(117) --> [X], { member(X, [250,218,249,217]) }.
char(46) --> [46].


espaco_branco --> whsp, espaco_branco.
espaco_branco --> whsp.
whsp --> [X], {X<48, X \=46 }.

pontuacao(X) :-
    X =< 47,
    X \= 32.
pontuacao(X) :-
    X >= 58,
    X =< 64.
pontuacao(X) :-
    X >= 91,
    X =< 96.
pontuacao(X) :-
    X >= 123,
    X =< 126.


writeCenas([], '').
writeCenas([X | Y], Output) :-
    atom_concat(X, ' ', Output0),
    writeCenas(Y, Output1),
    atom_concat(Output0, Output1, Output).

makerestr([],_,[]).
makerestr([X | Y],Restr,[H | T]) :-
    H = Restr-X,
    makerestr(Y, Restr, T).

% remove_dups(+List, -NewList)
% New List isbound to List, but with duplicate items removed.

removeDups([], []).
removeDups([F | H], R) :-
    member(F, H),
    remove_dups(H, R).
removeDups([F | H], [F | R]) :-
    \+ member(F, H),
    remove_dups(H, R).
