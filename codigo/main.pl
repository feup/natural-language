:- dynamic erro/1.
:- dynamic context/2.

:- use_module(library(sockets)),
   use_module(library(lists)),
   consult('auxiliares.pl'),
   consult('base_de_conhecimento.pl'),
   consult('gramatica.pl'),
   consult('answerData.pl'),
   consult('respostas.pl'),
   consult('perguntas.pl').

port(60070).

% launch me in sockets mode
server:-
    port(Port),
    socket_server_open(Port, Socket),
    socket_server_accept(Socket, _Client, Stream, [type(text)]),
    write('Accepted connection'), nl,
    serverLoop(Stream),
    socket_server_close(Socket).

% wait for commands
serverLoop(Stream) :-
    repeat,
    read(Stream, ClientMsg),
    write('Received: '), write(ClientMsg), nl,
    parse_input(ClientMsg, MyReply),
    format(Stream, '~q.~n', [MyReply]),
    write('Wrote: '), write(MyReply), nl,
    flush_output(Stream),
    (ClientMsg == quit; ClientMsg == end_of_file), !.

parse_input(le_frase(L),Resposta):-
    write('Estou aqui!'), nl,
    le_frase(L,Resposta).

parse_input(quit, ok-bye) :- !.
