resposta(fa-_, Sujeito-Valor, Restrs, 'Sim') :-
    checkAnswer(Sujeito-Valor, Restrs), !.
resposta(fa-_, _-_, _, 'Nao') :- !.
resposta(Q-P, Sujeito, Restrs, Resposta) :-
    Q \= fa,
    answer(Sujeito, Restrs, Resultado), !,
    length(Resultado, N),
    respostaaux(Q-P, Sujeito, Resultado, N, Resposta).

respostaaux(ql-_, _, _, 0, 'Nao foram encontrados resultados') :-
    !.
respostaaux(ql-s, Sujeito, Resultado, 1, Resposta) :-
    !,
    nome(G-s, Sujeito, At),
    determinante(G, s, X),
    writeCenas(Resultado, Aux),
    atom_concat(X, ' ', R0),
    atom_concat(R0, At, R1),
    atom_concat(R1, ' e ', R2),
    atom_concat(R2, Aux, Resposta).
respostaaux(ql-p, Sujeito, Resultado, N, Resposta) :-
    N \= 1, !,
    nome(G-p, Sujeito, At),
    determinante(G, p, X),
    writeCenas(Resultado, Aux),
    atom_concat(X, ' ', R0),
    atom_concat(R0, At, R1),
    atom_concat(R1, ' sao ', R2),
    atom_concat(R2, Aux, Resposta).
respostaaux(qt-Num, media, Resultado, N, Resposta) :-
    !, respostaaux(ql-Num, media, Resultado, N, Resposta), !.
respostaaux(qt-s, _, _, N, N) :-
    N = 1, !.
respostaaux(qt-p, _, _, N) :-
    N \= 1, !.
respostaaux(_-_, _, _, _, 'Nao concordancia em numero') :-
    !.
