answer(Sujeito, Restrs, Result) :-
	decompose(Restrs, NewRestrs),
	answerAux(Sujeito, NewRestrs, R0),
	removeDups(R0, Result).

answerAux(_, [], []).
answerAux(Sujeito, [Restrs | T0], Result) :-
	find(Sujeito, Restrs, R0),
	answerAux(Sujeito, T0, R1),
	append(R0, R1, Result).

checkAnswer(Sujeito-Answer, Restrs) :-
	find(Sujeito, Restrs, Answers), !,
	member(Answer, Answers), !.


decompose(Restrs, TrimmedRestrs) :-
	getDecomposePlaceHolder(Restrs, Placeholder), !,
	findall(
		TrimmedRestr,
		decomposeAux(Placeholder, Restrs, TrimmedRestr),
		TrimmedRestrs
	).

decomposeAux([], _, []).
decomposeAux([Tipo-X | T], Restrs, TrimmedRestrs) :-
	member(Tipo-X, Restrs),
	decomposeAux(T, Restrs, TR0),
	append([Tipo-X], TR0, TrimmedRestrs).


getDecomposePlaceHolder([], []).
getDecomposePlaceHolder([Tipo-_ | H], NewRestrs) :-
	getDecomposePlaceHolder(H, NR0),
	(
		(member(Tipo-_, NR0), !, append([], NR0, NewRestrs));
		append([Tipo-_], NR0, NewRestrs)
	).



find(Sujeito, Restrs, Resultado) :-
	find_aux(Sujeito, Restrs, R0),
	length(Restrs, N),
	findall(
		Item,
		(
			count(R0, Item, N)
		),
		Resultado
	).

find_aux(Sujeito, [Restr], Resultado) :-
	!, findForOne(Sujeito, Restr, Resultado).
find_aux(Sujeito, [Restr | T], Resultado) :-
	findForOne(Sujeito, Restr, R0),
	find_aux(Sujeito, T, R1),
	append(R0, R1, Resultado).

findForOne(Sujeito, Restr, Resultado) :-
	findall(
		Item,
		(
			verifyRestr(Sujeito, Restr, Item),
			nonvar(Item)
		),
		Resultado
	).


% Verificar Restrição

verifyRestr(_, [], _).
% Regiao/Universidade - Curso
verifyRestr(Sujeito, curso-Sigla, Nome) :-
	member(Sujeito, [regiao, universidade]),
	!,
	curso(_, [Sigla, _], Atributos),
	findAttribute(faculdade, Atributos, SiglaFaculdade),
	verifyRestr(Sujeito, faculdade-SiglaFaculdade, Nome).
% Regiao
verifyRestr(regiao, Tipo-Valor, Nome) :-
	!,
	Facto=..[Tipo, _, [Valor, _], Atributos],
	Facto,
	findAttribute(regiao, Atributos, Nome).
% Media
verifyRestr(media, curso-Sigla, Valor) :-
	!,
	curso(_, [Sigla, _], Atributos),
	findAttribute(media, Atributos, Valor).
% Curso - Regiao\Universidade
verifyRestr(curso, Tipo-Nome, Sigla) :-
	member(Tipo, [regiao, universidade]),
	!,
	faculdade(_, [SiglaFaculdade, _], AtributosFaculdade),
	checkAttribute(Tipo-Nome, AtributosFaculdade),
	curso(_, [Sigla, _], Atributos),
	checkAttribute(faculdade-SiglaFaculdade, Atributos).
% Universidades - Faculdades
verifyRestr(universidade, faculdade-SiglaFaculdade, Sigla) :-
	!,
	faculdade(_, [SiglaFaculdade, _], Atributos),
	checkAttribute(universidade-Sigla, Atributos).
% Faculdades, Universidades e Cursos
verifyRestr(Sujeito, Restr, Sigla) :-
	member(Sujeito, [faculdade, universidade, curso]), !,
	Facto=..[Sujeito, _, [Sigla, _], Atributos],
	Facto,
	checkAttribute(Restr, Atributos).
% Media Mais Baixa/Mais Alta
verifyRestr(Sujeito, Restr, Valor) :-
	member(Sujeito, ['media<', 'media>']),
	!,
	findForOne(curso, Restr, Cursos),
	getMean(Sujeito, Cursos, _-Valor).


% Verifica se respeita os atributos

% Medias
checkAttribute(media-Valor, Atributos) :-
	!, findAttribute(media, Atributos, V0),
	convert(V0, N0),
	N0 = Valor.
checkAttribute('media<'-Valor, Atributos) :-
	!, findAttribute('media', Atributos, V0),
	convert(V0, N0),
	N0 < Valor.
checkAttribute('media>'-Valor, Atributos) :-
	!, findAttribute('media', Atributos, V0),
	convert(V0, N0),
	N0 > Valor.
% Generico
checkAttribute(Tipo-Valor, Atributos) :-
	member(Tipo-Valor, Atributos).
% Regioes Mae
checkAttribute(regiao-Regiao, Atributos) :-
	distrito(_, NovoValor, Regioes),
	member(Regiao, Regioes),
	member(regiao-NovoValor, Atributos).


% Devolve valor do tipo

findAttribute(_, [], _).
findAttribute(Tipo, [Tipo-Valor | _], Valor) :- !.
findAttribute(Tipo, [_ | T], Valor) :-
	findAttribute(Tipo, T, Valor).


% Count

count(List, Element, Count) :-
    sort(List, List1),
    member(Element, List1),
    countElement(List, Element, Count).

countElement([], _, 0).
countElement([Element | T], Element, Count) :-
	countElement(T, Element, C1),
	Count is C1 + 1.
countElement([H | T], Element, Count) :-
	H \= Element,
	countElement(T, Element, Count).

% Get Mean

getMean(_, [], _) :- !, fail.
getMean(Comp, [Sigla | T], SiglaRes-Value) :-
	curso(_, [Sigla, _], Atributos),
	findAttribute(media, Atributos, V0),
	getMean(Comp, T, Sigla1-V1), !,
	getMeanAux(Comp, Sigla-V0, Sigla1-V1, SiglaRes-Value).
getMean(_, [Sigla], Sigla-Value) :-
	curso(_, [Sigla, _], Atributos),
	findAttribute(media, Atributos, Value).

getMeanAux('media<', Sigla-V0, _-V1, Sigla-V0) :- convert(V0, N0), convert(V1, N1), N0 < N1.
getMeanAux('media<', _-V0, Sigla-V1, Sigla-V1) :- convert(V0, N0), convert(V1, N1), N0 >= N1.
getMeanAux('media>', Sigla-V0, _-V1, Sigla-V0) :- convert(V0, N0), convert(V1, N1), N0 > N1.
getMeanAux('media>', _-V0, Sigla-V1, Sigla-V1) :- convert(V0, N0), convert(V1, N1), N0 =< N1.

convert(String, Number) :-
	atom_codes(String, X),
	number_codes(Number, X).