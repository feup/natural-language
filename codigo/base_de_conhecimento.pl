regiao(m-s, norte).
regiao(m-s, centro).
regiao(m-s, sul).

distrito(m-s, porto, [norte]).
distrito(i-s, lisboa, [centro]).

universidade(f-s, [up, [universidade, do, porto]], [regiao-porto]).
universidade(f-s, [ul, [universidade, de, lisboa]], [regiao-lisboa]).

faculdade(f-s, [feup, [faculdade, de, engenharia, da, universidade, do, porto]], [regiao-porto, ano-1926, universidade-up, tipo-engenharia]).
faculdade(f-s, [fcup, [faculdade, de, ciencias, da, universidade, do, porto]], [ regiao-porto, ano-1911, universidade-up, tipo-ciencias]).
faculdade(f-s, [fmul, [faculdade, de, medicina, da, universidade, de, lisboa]], [regiao-lisboa, ano-1911, universidade-ul, tipo-medicina]).
faculdade(f-s, [flul, [faculdade, de, letras, da, universidade, de, lisboa]], [regiao-lisboa, ano-1911, universidade-ul, tipo-letras]).

tipo(i-s, engenharia).
tipo(i-s, medicina).
tipo(i-s, enfermagem).
tipo(i-s, letras).

curso(m-s, ['mieic', [mestrado, integrado, em, engenharia, informatica, e, de, computacao]], [faculdade-feup, tipo-engenharia, media-'15.8']).
curso(m-s, ['miem', [mestrado, integrado, em, engenharia, mecanica]], [faculdade-feup, tipo-engenharia, media-'17.0']).
curso(m-s, ['mieec', [mestrado, integrado, em, engenharia, eletrotenica, e, computadores]], [faculdade-feup, tipo-engenharia, media-'14.8']).
curso(m-s, ['mi:ef', [mestrado, integrado, em, engenharia, fisica]], [faculdade-fcup, tipo-ciencias, media-'14.5']).
curso(m-s, ['mi:ers', [mestrado, integrado, em, engenharia, de, redes, e, sistemas, informaticos]], [faculdade-fcup, tipo-ciencias, media-'14.0']).
curso(m-s, ['md', [mestrado, em, medicina]], [faculdade-fmul, tipo-medicina, media-'18.4']).
curso(m-s, ['fl', [licenciatura, em, filosofia]], [faculdade-flul, tipo-letras, media-'10.8']).

%% relation(curso, universidade, faculdade).



determinante(f, p, 'as').
determinante(f, s, 'a').
determinante(m, s, 'o').
determinante(m, p, 'os').

nome(f-s, 'regiao', 'regiao').
nome(f-p, 'regiao', 'regiões').
nome(m-s, 'curso', 'curso').
nome(m-p, 'curso', 'cursos').
nome(f-s, 'media', 'media').
nome(f-p, 'media', 'medias').
nome(f-s, 'media<', 'media mais baixa').
nome(f-s, 'media>', 'media mais alta').
nome(f-s, 'faculdade', 'faculdade').
nome(f-p, 'faculdade', 'faculdades').
nome(f-s, 'universidade', 'universidade').
nome(f-p, 'universidade', 'universidades').
