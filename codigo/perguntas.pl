le_frase :-
    write('Escreva uma frase: '),
    read_line(Frase),
    %removePontuacao(F0, Frase),
    lista_de_palavras(Lista_Pal, Frase, []), !,
    (   frase(Q-N, Sujeito, Restr, Lista_Pal, []), !, resposta(Q-N, Sujeito, Restr, Resposta), !, write(Resposta), nl;
        (   erro(semantico), !, write('erro semantico'), nl, nl;
            write('erro sintatico'), nl, nl
        )
    ).

le_frase(ClientMsg, Resposta):-
    write(ClientMsg), nl,
    (   frase(Q-N, Sujeito, Restr, ClientMsg, []), !, resposta(Q-N, Sujeito, Restr, Resposta), !, write(Resposta), nl;
        (   erro(semantico), !, write('erro semantico'), nl, nl;
            write('erro sintatico'), nl, nl
        )
    ).

% Frases

frase(Q-N, Sujeito, Restr) -->
    frase_afirmativa(Q-N, Sujeito, Restr).
frase(Q-N, Sujeito, Restr) -->
    frase_interrogativa(Q-N, Sujeito, Restr).

%
% Frases Afirmativas
%

frase_afirmativa(fa-N, SujeitoTipo-SujeitoValor, Restr) -->
    (
        (det(G-N), atributo(SujeitoTipo, G-N, _, SujeitoValor));
        (([e];[ou]), { context(fa-N, SujeitoTipo-SujeitoValor) })
    ),
    (
        (verbo(N, ser), complementoSimples(fa, SujeitoTipo, Restr, 1));
        ((verbo(N, ter);verbo(N,possuir)), sintg_verbal_media(SujeitoTipo, Restr))
    ),
    { assert(context(fa-N, SujeitoTipo-SujeitoValor)) },
    { write([fa-N, SujeitoTipo-SujeitoValor, Restr]), nl, nl }.
frase_afirmativa(fa-N, SujeitoTipo-SujeitoValor, [RestrTipo-RestrValor]) -->
    (
        (det(G-N), atributo(RestrTipo, G-N, _, RestrValor));
        ([e];[ou], { context(fa-N, RestrTipo-RestrValor) })
    ),
    (verbo(N, ter);verbo(N, possuir)),
    complementoSimples(fa, RestrTipo, [SujeitoTipo-SujeitoValor], 2),
    { assert(context(fa-N, SujeitoTipo-SujeitoValor)) },
    { write([fa-N, SujeitoTipo-SujeitoValor, [RestrTipo-RestrValor]]), nl, nl }.

%
% Frases Interrogativas
%

frase_interrogativa(Q-N, Sujeito, Restr) -->
    [e],
    { context(Q-N, Sujeito) },
    sintg_verbal_mult(_-N, Sujeito, Restr, _-_, _),
    { write([Q-N, Sujeito, Restr]), nl, nl }.

frase_interrogativa(Q-N, Sujeito, Restr) -->
    sintg_interrogativo(G-N, Q-Tipo, ObrDet, VerbPres),
    frase_interrogativa_aux(G-N, Q-Tipo, ObrDet, VerbPres, Sujeito, Restr),
    { assert(context(Q-N, Sujeito)), write([Q-N, Sujeito, Restr]), nl, nl }.

frase_interrogativa_aux(G-N, Q-Tipo, ObrDet, VerbPres, Sujeito, Restr) -->
    sintg_nominal(G-N, Sujeito, ObrDet),
    adjectivo(G-N, Sujeito, R0),
    complemento(Sujeito, R1),
    { append(R0, R1, Restr0) },
    sintg_verbal_mult(G-N, Sujeito, Restr1, Q-Tipo, VerbPres),
    { append(Restr0, Restr1, Restr) }.
frase_interrogativa_aux(G-N, Q-Tipo, ObrDet, VerbPres, Sujeito, Restr) -->
    sintg_nominal(G-N, Sujeito, ObrDet),
    complemento(Sujeito, Restr0),
    sintg_verbal_mult(G-N, Sujeito, Restr1, Q-Tipo, VerbPres),
    { append(Restr0, Restr1, Restr) }.
frase_interrogativa_aux(G-N, Q-Tipo, ObrDet, VerbPres, Sujeito, Restr) -->
    sintg_nominal(G-N, Sujeito, ObrDet),
    adjectivo(G-N, Sujeito, R0),
    sintg_verbal_mult(G-N, Sujeito, R1, Q-Tipo, VerbPres),
    { append(R0, R1, Restr) }.
frase_interrogativa_aux(G-N, Q-Tipo, ObrDet, VerbPres, Sujeito, Restr) -->
    sintg_nominal(G-N, Sujeito, ObrDet),
    sintg_verbal_mult(G-N, Sujeito, Restr, Q-Tipo, VerbPres).
frase_interrogativa_aux(G-N, _-_, ObrDet, _, Sujeito, Restr) -->
    sintg_nominal(G-N, Sujeito, ObrDet),
    adjectivo(G-N, Sujeito, Restr).


%
% Sintagma Interrogativo
%

sintg_interrogativo(G-N, Q-Tipo, 1, 1) --> % 1 -> Det. Obrigatorio, 1 -> Verbo presente
    pron_int(G-N, Q-Tipo, _, 1),
    verbo(N, ser).
sintg_interrogativo(G-N, Q-Tipo, ObrDet, 0) -->
    pron_int(G-N, Q-Tipo, ObrDet, _).

%
% Sintagma Nominal
%

sintg_nominal(G-N, Sujeito, 1)-->
    det(G-N),
    nome(G-N, Sujeito).
sintg_nominal(G-N, Sujeito, 0) -->
    nome(G-N, Sujeito).

%
% Sintagma Verbal Multiplo
%

sintg_verbal_mult(G-N, Sujeito, Restr, Q-Tipo, VerbPres) -->
    sintg_verbal(G-N, Sujeito, R0, Q-Tipo, VerbPres),
    sintg_verbal2(G-N, Sujeito, R1),
    { append(R0, R1, Restr) }.
sintg_verbal_mult(G-N, Sujeito, Restr, Q-Tipo, VerbPres) -->
    sintg_verbal(G-N, Sujeito, Restr, Q-Tipo, VerbPres).

%
% Sintagma Verbal Simples
%

% Que
sintg_verbal(_-N, Sujeito, Restr, _-0, _) -->
    verbo(N, ser),
    complemento(Sujeito, Restr).
% Qual
sintg_verbal(_-N, Sujeito, Restr, _-1, _) -->
    [que],
    verbo(N, ser),
    complemento(Sujeito, Restr).
sintg_verbal(_-_, Sujeito, Restr, _-1, _) -->
    complemento(Sujeito, Restr).
% Quantas
sintg_verbal(_-_, media, _, _-2, _) --> !.
sintg_verbal(_-_, 'media<', _, _-2, _) --> !.
sintg_verbal(_-_, 'media>', _, _-2, _) --> !.
sintg_verbal(_-N, Sujeito, Restr, _-2, 0) -->
    verbo(N, ser),
    complemento(Sujeito, Restr).
sintg_verbal(_-N, Sujeito, Restr, _-2, 1) -->
    [que],
    verbo(N, ser),
    complemento(Sujeito, Restr).
sintg_verbal(_-_, Sujeito, Restr, _-2, 1) -->
    complemento(Sujeito, Restr).
% Com media __ a X
sintg_verbal(_-_, Sujeito, Restr, _-Tipo, _) -->
    {Tipo \= 0},
    [com],
    sintg_verbal_media(Sujeito, Restr).

% Sintagma Verbal - Media

sintg_verbal_media(Sujeito, [Nome-Valor]) -->
    sintg_verbal_media_certa(Sujeito, [Nome-Valor]).
sintg_verbal_media(Sujeito, [Nome1-Valor1, Nome2-Valor2]) -->
    sintg_verbal_media1(Sujeito, [Nome1-Valor1]),
    sintg_verbal_media2(Sujeito, [Nome2-Valor2], Nome1).
sintg_verbal_media(Sujeito, [Nome-Valor]) -->
    sintg_verbal_media1(Sujeito, [Nome-Valor]).

sintg_verbal_media_certa(Sujeito, [media-Valor]) -->
    nome(_-s, media),
    [de],
    [Valor],
    {
        (relacao(L, Sujeito, media), member(_,L)) ;
        assert(erro(semantico)), fail
    }.
sintg_verbal_media1(Sujeito, [Nome-Valor]) -->
    nome(_-s, media),
    (
        ([superior], {Nome='media>'});
        ([inferior], {Nome='media<'})
    ),
    [a],
    [Valor],
    {
        (relacao(L, Sujeito, Nome), member(_,L)) ;
        assert(erro(semantico)), fail
    }.
sintg_verbal_media2(Sujeito, [Nome-Valor], Nome0) -->
    [e],
    ([media];[]),
    (
        ([superior], {Nome='media>'});
        ([inferior], {Nome='media<'})
    ),
    [a],
    [Valor],
    {
        (Nome \= Nome0, relacao(L, Sujeito, Nome), member(_,L)) ;
        assert(erro(semantico)), fail
    }.


%
% 2º Sintagma Verbal
%

sintg_verbal2(G-N, Sujeito, Restr) -->
    %([e];[]),
    [com],
    sintg_verbal_media(Sujeito, R0),
    sintg_verbal2(G-N, Sujeito, R1),
    { append(R0, R1, Restr) }.
sintg_verbal2(G-N, Sujeito, Restr) -->
    [que],
    verbo(N, ser),
    complemento(Sujeito, R0),
    sintg_verbal2(G-N, Sujeito, R1),
    { append(R0, R1, Restr) }.
sintg_verbal2(G-N, Sujeito, Restr) -->
    ([e];[ou]),
    verbo(N, ser),
    complemento(Sujeito, R0),
    sintg_verbal2(G-N, Sujeito, R1),
    { append(R0, R1, Restr) }.
sintg_verbal2(_-_, _, [], _, _).


%
% complemento
%

complemento(Sujeito, Restr) -->
    complementoSimples(q, Sujeito, [Nome-Valor], 1),
    complementoAux(Sujeito, [T]),
    { append([Nome-Valor], [T], Restr) }.
complemento(Sujeito, [Nome-Valor]) -->
    complementoSimples(q, Sujeito, [Nome-Valor], 1).

complementoAux(Sujeito, [Nome-Valor]) -->
    ([e];[ou]),
    complementoSimples(q, Sujeito, [Nome-Valor], 0).
complementoAux(Sujeito, Restr) -->
    % [','],
    complementoSimples(q, Sujeito, [Nome-Valor], 0),
    complementoAux(Sujeito, [T]),
    { append([Nome-Valor], [T], Restr) }.

%
% Simples
%


complementoSimples(Q, Sujeito, [Nome-Valor], X) -->
    (
        (contr(G-N, T), {X \= 2});
        ([], {X = 1});
        (det(G-N), {X = 2})
    ),
    atributo(Nome, G-N, T, Valor),
    {
        (relacao(L, Sujeito, Nome), member(Q,L)) ;
        assert(erro(semantico)), fail
    }.

%
% Adjectivo
%

adjectivo(G-N, Sujeito, [Nome-Valor]) -->
    adjectivo(G-N, Nome-Valor),
    {
        (relacao(L, Sujeito, Nome), member(_,L)) ;
        assert(erro(semantico)), fail
    }.
